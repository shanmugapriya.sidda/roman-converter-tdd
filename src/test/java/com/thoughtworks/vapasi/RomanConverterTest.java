package com.thoughtworks.vapasi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RomanConverterTest {

    private RomanConverter romanConvertor;

    @BeforeEach
    void setUp() {
        romanConvertor = new RomanConverter();
    }

    @ParameterizedTest
    @CsvSource({"I,1","II,2","III,3","IV,4","V,5","VI,6","VII,7","VIII,8","IX,9","X,10","XXXVI,36","MMXII,2012","MCMXCVI,1996"})
     void shouldConvertI(String roman,int value) {
        assertEquals(value,romanConvertor.convertRomanToArabicNumber(roman));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenInvalidRomanValueIsPassed() {
        assertThrows(IllegalArgumentException.class,() -> {romanConvertor.convertRomanToArabicNumber("9");});
    }
}