package com.thoughtworks.vapasi;

import java.util.HashMap;
import java.util.Map;

public class RomanConverter {

    private static final Map<Character, Integer> inputMap = new HashMap<Character, Integer>() {
        {
            put('I', 1);
            put('V', 5);
            put('X', 10);
            put('L', 50);
            put('C', 100);
            put('M', 1000);
            put('D', 500);
        }
    };


    public Integer convertRomanToArabicNumber(String roman) {
        int currentValue = 0;
        int previousValue = 0;
        int result = 0;

        for (int i = roman.length() - 1; i >= 0; i--) {
            try {
                currentValue = getValue(roman, i);
            } catch (Exception e) {
                throw new IllegalArgumentException();
            }
            if (currentValue < previousValue) {
                result -= currentValue;
            } else {
                result += currentValue;
            }
            previousValue = currentValue;
        }

        return result;
    }

    private Integer getValue(String roman, int i) {
        return inputMap.get(roman.charAt(i));
    }
}
